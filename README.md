# Power Through Python Certification 


## Authors
This tutorial was written by 
* Jeff Elkner
* Adrian Buchholz
* Francesco Crisafulli 
* Peter Yuan 


## Build Instructions
Install dependencies with `pip install -r requirements.txt` and then build with
`runestone build`. The book will be available inside the
``build/powerthroughpycert`` subdirectory.

## License

Copyright © Jeff Elkner, Adrian Buchholz, Francesco Crisafulli, and Peter Yuan 

Permission is granted to copy, distribute and/or modify this document under the
terms of the GNU Free Documentation License, Version 1.3 or any later version
published by the Free Software Foundation; with no Invariant Sections, no
Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is included
in the file "LICENSE.txt".
