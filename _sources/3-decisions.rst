Descions and Loops
==================


Included Skills
---------------

Be able to:

* read, write, and analyze conditional branching statements,
  ``if``, ``if else``, and ``if elif ... elif else``.
* read, write, and analyze nested branching statements.
* read, write, and analyze ``for`` loops.
* evalute ``range`` function calls. 
* read, write, and analyze ``while`` loops.
* correctly evaluate and use ``pass``, ``break``, and ``continue`` statements. 
* correctly evaluted nested looping and branching statements.
