..  Copyright (C) Jeff Elkner, Adrian Buchholz, Francesco Crisafulli, and
    Peter Yuan. Permission is granted to copy, distribute and/or modify this
    document under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List, no
    Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is
    included in the section entitled "GNU Free Documentation License".

Where to Learn Python
=====================

.. index:: Python, learning Python, Python resources

Assumptions
-----------

These practice problems are designed specifically to help you prepare for a
certifcation exam. They are not designed to teach you Python, which we assume
here you already know. We will provide discussion on each of the sample
problems that can help reinforce your Python skills, but if you are new to
Python you should go elsewhere to learn it before coming back here to begin
practicing for the certification exam.


Open Book Project (OBP) Resources
---------------------------------

The following is an anotated list of `open educational resources
<https://en.wikipedia.org/wiki/Open_educational_resources>`_ hosted here on the
`Open Book Project <http://www.openbookproject.net>`_ webiste.

* `CS Principles: Big Ideas in Programming
  <http://www.openbookproject.net/books/StudentCSP>`_ - An interactive,
  online textbook created at at the `Georgia Tech College of Computing
  <https://en.wikipedia.org/wiki/Georgia_Institute_of_Technology_College_of_Computing>`_.
  Designed to prepare students for the `AP Computer Science Principles
  <https://en.wikipedia.org/wiki/AP_Computer_Science_Principles>`_ exam, it
  uses `Skulpt <http://www.skulpt.org>`_ to require only a
  `JavaScript <https://en.wikipedia.org/wiki/JavaScript>`_ enabled browser for
  use. The OBP version is a remix with the added goal of having all examples
  run unchanged in `CPython <https://en.wikipedia.org/wiki/CPython>`_.
* `How to Think Like a Computer Scientist: Learning with Python 3 (RLE)
  <http://openbookproject.net/thinkcs/python/english3e/>`_ - An introduction
  to computer programming with a computer science focus using Python 3. 
* `GASP Python Course <http://www.openbookproject.net/pybiblio/gasp/course>`_ -
  A beginners course in Python 3 using a game module enabling beginners to
  write 1980s style arcade games.


Other Resources
---------------
* `Python.org <https://www.python.org>`_ - The official Python community
  website has a wealth of resource links in its documention section. Of
  particular relevance here are the resources linked from the
  `Beginner's Guide to Python wiki page
  <https://wiki.python.org/moin/BeginnersGuide>`_.
* `SoloLearn Python 3 Tutorial <https://www.sololearn.com/Course/Python>`_ -
  This provides a clear, accessible, yet thorough hands-on introduction to
  Python.
* `Coursera Python for Everybody Specialization
  <https://www.coursera.org/specializations/python>`_ - Dr. Chuck materials
  prepare learners to use Python in data science in a learner friendly way.
