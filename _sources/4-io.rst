Input and Output
================


Included Skills
---------------

Be able to:

* evaluate and use ``open`` and ``close`` functions to connect to files in the
  host file system.
* evaluate and use the ``read`` method to get data from a file.
