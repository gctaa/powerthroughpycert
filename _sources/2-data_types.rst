.. Copyright (C) Jeff Elkner, Adrian Buchholz, Francesco Crisafulli, and
   Peter Yuan. Permission is granted to copy, distribute and/or modify this
   document under the terms of the GNU Free Documentation License, Version 1.3
   or any later version published by the Free Software Foundation; with
   Invariant Sections being Forward, Prefaces, and Contributor List, no
   Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is
   included in the section entitled "GNU Free Documentation License".

.. shortname: Data Types and Operators

.. description: int, float, bool, str, and list data types

.. setup for automatic question numbering

.. qnum::
    :start 1


Data Types and Operators
========================


Included Skills
---------------

Be able to:

* readily recognize Python's ``int``, ``float``, ``bool``, ``str``, and
  ``list`` data types.
* evaluate expressions and identify the resulting data type.
* perform indexing and slicing operations on sequence types.
* evaluate expressions involving identity (``is``) and containment (``in``).
* convert from one data type to another.
* evaluate expressions based on operator precedence.
* select operators appropriately to achieve a desired result.


.. mchoice:: W1Q1
    :answer_a: Yes
    :answer_b: No
    :correct: b
    :feedback_a: No, Python is dynamically typed. Datatypes do not need to be declared.
    :feedback_b: That's right.

    Variables must be declared with a datatype in Python.


.. mchoice:: W1Q2
    :answer_a: Yes
    :answer_b: No
    :correct: a
    :feedback_a: Correct. ``int`` and ``float`` are seperate types.
    :feedback_b: No, ``int`` and ``float`` are seperate types. 

    Python has seperate types for integers and floating point values. 


.. mchoice:: W1Q3
    :answer_a: Yes
    :answer_b: No
    :correct: a
    :feedback_a: Correct. ``True`` and ``False`` must begin with capital letters. 
    :feedback_b: ``True`` and ``False`` are Python's boolean values, and they must begin with capital letters. 

    Python's boolean values begin with capital letters. 

    
.. mchoice:: W1Q4
    :answer_a: Only I 
    :answer_b: Only II 
    :answer_c: I and III
    :answer_d: Only III 
    :answer_e: II and IV
    :correct: c
    :feedback_a: Try again.
    :feedback_b: Try again.
    :feedback_c: You got it! 
    :feedback_d: Try again.
    :feedback_e: Try again.

    Given the following code:

    ::

        fruits = ['apples', 'pears', 'grapes', 'bananas', 'cherries', 'kiwis']
        fruits.sort()

    Which of the following will display pears?

    I.   ``print(fruits[-1])``
    II.  ``print(fruits[1])``
    III. ``print(fruits[5])``
    IV.  ``print(fruits[6])``


.. mchoice:: W1Q5
    :answer_a: Yes
    :answer_b: No
    :correct: a
    :feedback_a: That’s right.
    :feedback_b: That’s not right, a Boolean must begin with a capital letter to run
    
    When setting a Boolean, does the value has to begin with a capital letter? 

.. mchoice:: W1Q6
    :answer_a: Example A
    :answer_b: Example B
    :answer_c: Example C
    :answer_d: Example D
    :correct: d
    :feedback_a: Try again.
    :feedback_b: Try again.
    :feedback_c: Try again.
    :feedback_d: You got it!


    Given the following code:
     
    A.   ``List = [A,B,C,D,E]``
    B.  ``List = [“A,B,C,D,E”]``
    C. ``List = (A,B,C,D,E)``
    D.  ``List = ["A","B","C","D","E"]``
    
    Which of the following us a proper way of creating a list in Python (With Strings)?

