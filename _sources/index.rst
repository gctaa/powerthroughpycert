..  Copyright (C) Jeff Elkner, Adrian Buchholz, Francesco Crisafulli, and
    Peter Yuan. Permission is granted to copy, distribute and/or modify this
    document under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation; with
    Invariant Sections being Forward, Prefaces, and Contributor List, no
    Front-Cover Texts, and no Back-Cover Texts.  A copy of the license is
    included in the section entitled "GNU Free Documentation License".

 
Powering Through Python Certification
=====================================

Introduction
------------

For better or worse, pursuing `professional certification
<https://en.wikipedia.org/wiki/Professional_certification>`_ has become an
unavoidable requirement in Virginia high school `Career and Technical Education
<https://www.edweek.org/ew/issues/career-technical-education/index.html>`_
programs (see the Virginia Department of Education's (VDOE) `The Path to
Industry Certification: High School Industry Credentialing
<http://www.doe.virginia.gov/instruction/career_technical/programs/path_industry_certification/>`_).
While I personally have my concerns with many of these "industry
certifications", both in terms of their quality and their relevancy to *real
learning*, in the spirit of "if you can't beat 'em, join 'em", we present to
you this home spun resource designed to help our programming students at the
Arlington Career Center meet their VDOE obligations.

Jeff Elkner, Arlington, Virginia


Worksheets
----------

* `Where to Learn Python <1-intro.html>`_ Read this first. It lays out
  expectations and good resources for learning Python. 

* `Data Types and Operators <2-data_types.html>`_ Test your ability to
  identify **str**, **int**, **float**, and **bool** data types and use
  appropriate operations with them.

* `Decisions and Loops <3-decisions.html>`_ Construct and analyze code segments
  using branching and looping statements. 

* `Input and Output <4-io.html>`_ Perform file and console input and output
  operations. 

* `Structuring and Documenting Code <5-structure.html>`_ A
  graphical game in which you try to outwit a horde of robots.

* `Troubleshooting and Error Handling <6-errors.html>`_ A gasp version of a
  classic arcade game.

* `Modules and Tools <7-modules.html>`__ Lists of things are important
  in Python. This sheet shows you some things you can do with them.


.. toctree::
    :maxdepth: 1
    :hidden:

    1-intro.rst
    2-data_types.rst
    3-decisions.rst
    4-io.rst
    5-structure.rst
    6-errors.rst
    7-modules.rst

.. toctree::
    :maxdepth: 1
    :hidden:

    fdl-1.3.rst
